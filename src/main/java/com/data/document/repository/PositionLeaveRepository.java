package com.data.document.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.data.document.model.PositionLeave;

@Repository
public interface PositionLeaveRepository extends JpaRepository<PositionLeave, Long> {

	@Query(nativeQuery=true, value="SELECT p.* FROM position_leave p WHERE p.position_id = ?1")
	PositionLeave findDataByPosition(Long position);
}
