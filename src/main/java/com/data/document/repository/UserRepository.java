package com.data.document.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.data.document.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByNik(BigDecimal nik);
	
	@Query(nativeQuery=true, value="SELECT * FROM users WHERE position_id = ?1")
	List<User> findUserByPosition(Long id);
}
