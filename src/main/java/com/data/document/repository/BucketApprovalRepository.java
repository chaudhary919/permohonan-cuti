package com.data.document.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.data.document.model.BucketApproval;

@Repository
public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Long> {

	@Query(nativeQuery=true, value="SELECT b.* FROM bucket_approval b " + 
			"INNER JOIN user_leave_request u ON b.user_id = u.user_id AND b.status = u.status WHERE b.status = ?1")
	BucketApproval findDataByStatus(String status);
	
	@Query(nativeQuery=true, value="SELECT b.* FROM bucket_approval b WHERE b.user_id = ?1 AND b.status = 'waiting' ")
	BucketApproval findDataExist(Long id);
	
	@Query(nativeQuery=true, value="SELECT b.* FROM bucket_approval b INNER JOIN user_leave_request u " + 
			"ON b.user_id = u.user_id AND b.status = u.status " + 
			"WHERE b.user_id = ?1 ORDER BY b.updated_date DESC")
	List<BucketApproval> findDataBucketAll(Long userId);
}
