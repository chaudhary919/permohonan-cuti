package com.data.document.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.data.document.model.UserLeaveRequest;

@Repository
public interface UserLeaveRequestRepository extends JpaRepository<UserLeaveRequest, Long>{

	@Query(nativeQuery= true, value="SELECT u.* FROM user_leave_request u WHERE u.user_id = ?1 ORDER BY u.updated_date DESC")
	List<UserLeaveRequest> findDataByUserId(Long userId);
	
	@Query(nativeQuery= true, value="SELECT b.* FROM user_leave_request b WHERE b.user_id = ?1")
	Page<UserLeaveRequest> findDataById(Long userId, Pageable pageable);

	@Query(nativeQuery=true, value="SELECT u.* FROM bucket_approval b " + 
			"INNER JOIN user_leave_request u ON b.user_id = u.user_id AND b.status = u.status WHERE b.status = 'waiting' AND b.user_id = ?1")
	UserLeaveRequest findDataByStatus(Long userId);
}
