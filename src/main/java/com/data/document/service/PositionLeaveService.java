package com.data.document.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.data.document.model.PositionLeave;
import com.data.document.repository.PositionLeaveRepository;

@Service
public class PositionLeaveService {

	@Autowired
	PositionLeaveRepository plRepo;
	
	public List<PositionLeave> getDataAll() {
		return plRepo.findAll();
	}
	
	public PositionLeave getDataById(Long id) {
		Optional<PositionLeave> pl = plRepo.findById(id);
		if (pl.isPresent()) {
			return pl.get();
		}else {
			return null;
		}
	}
	
	public void inputData(PositionLeave pl) {
		plRepo.save(pl);
	}
	
	public void deleteData(PositionLeave pl) {
		plRepo.delete(pl);
	}
}
