package com.data.document.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.data.document.model.Position;
import com.data.document.repository.PositionRepository;

@Service
public class PositionService {

	@Autowired
	PositionRepository positionRepo;
	
	public List<Position> getDataAll() {
		return positionRepo.findAll();
	}
	
	public Position getDataById(Long id) {
		Optional<Position> pst = positionRepo.findById(id);
		if (pst.isPresent()) {
			return pst.get();
		}else {
			return null;
		}
	}
	
	public void inputData(Position pst) {
		positionRepo.save(pst);
	}
	
	public void deleteData(Position pst) {
		positionRepo.delete(pst);
	}
}
