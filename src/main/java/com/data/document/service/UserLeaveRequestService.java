package com.data.document.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.data.document.model.UserLeaveRequest;
import com.data.document.repository.UserLeaveRequestRepository;

@Service
public class UserLeaveRequestService {

	@Autowired
	UserLeaveRequestRepository ulrRepo;
	
	public List<UserLeaveRequest> getListDoc(Long id){
		return ulrRepo.findDataByUserId(id);
	}
	
	public Page<UserLeaveRequest> getListDocPage(Long userId, int page, int size){
		Pageable pageable = PageRequest.of(page, size, new Sort(Sort.Direction.DESC, "updated_date"));
		return ulrRepo.findDataById(userId, pageable);
	}
}
