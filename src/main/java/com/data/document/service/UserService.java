package com.data.document.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.data.document.model.User;
import com.data.document.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepo;
	
	public List<User> getDataAll() {
		return userRepo.findAll();
	}
	
	public User getDataById(Long id) {
		Optional<User> user = userRepo.findById(id);
		if (user.isPresent()) {
			return user.get();
		}else {
			return null;
		}
	}
	
	public void inputData(User user) {
		userRepo.save(user);
	}
	
	public void deleteData(User user) {
		userRepo.delete(user);
	}
}
