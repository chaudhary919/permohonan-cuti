package com.data.document.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.data.document.dto.BucketApprovalDto;
import com.data.document.model.BucketApproval;
import com.data.document.model.UserLeaveRequest;
import com.data.document.repository.BucketApprovalRepository;
import com.data.document.repository.UserLeaveRequestRepository;
import com.data.document.util.ConvertDate;

@RestController
@RequestMapping("/bucketApproval")
public class BucketApprovalController {

	@Autowired
	BucketApprovalRepository baRepo;
	
	@Autowired
	UserLeaveRequestRepository ulrRepo;
	
	@PostMapping("/resolveRequestLeave")
	public ResponseEntity<HashMap<String, Object>> insertData(@RequestBody BucketApprovalDto dto) throws ParseException {
		HashMap<String, Object> data = new HashMap<>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date now = new Date();
		try {
			Optional<BucketApproval> ba = baRepo.findById(dto.getIdBucket());
			
			if (ba.isPresent()) {
				UserLeaveRequest dataBa = ulrRepo.findDataByStatus(ba.get().getUser().getIdUser());
				Date resolvedDate = sdf.parse(ConvertDate.convertDate(dto.getResolvedDate().toString()));
				if (!ba.get().getStatus().trim().equals("waiting")) {
					data.put("Status", HttpStatus.OK);
					data.put("message", "Kesalahan Data, Status permohonan sudah diputuskan");
				}else {
					if (resolvedDate.compareTo(dataBa.getLeaveDateFrom()) < 0) {
						data.put("Status", HttpStatus.OK);
						data.put("message", "Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti");
					}else {
						ba.get().setStatus(dto.getStatus());
						ba.get().setResolverReason(dto.getResolverReason());
						ba.get().setResolvedBy(dto.getResolvedBy());
						ba.get().setResolvedDate(dto.getResolvedDate());
						ba.get().setUpdatedDate(now);
						baRepo.save(ba.get());
						
						if (dto.getStatus().trim().equals("reject")) {
							Date dateFrom = dataBa.getLeaveDateFrom();
							Date dateTo = dataBa.getLeaveDateTo();
							long diffInMillies = Math.abs(dateTo.getTime() - dateFrom.getTime());
						    long totalLeave = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
						    Integer value = (int) (long) totalLeave;
							dataBa.setRemainingDaysOff(dataBa.getRemainingDaysOff()+value);
						}
						dataBa.setUpdatedDate(now);
						dataBa.setStatus(dto.getStatus());
						ulrRepo.save(dataBa);
						
						data.put("Status", HttpStatus.OK);
						data.put("message", "Permohonan dengan ID "+dto.getIdBucket()+" telah berhasil diputuskan.");
					}
				}
				
			}else {
				data.put("Status", HttpStatus.OK);
				data.put("message", "Permohonan dengan ID "+dto.getIdBucket()+" Tidak Ditemukan");
			}
		} catch (Exception e) {
			data.put("Status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
}
