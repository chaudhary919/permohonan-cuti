package com.data.document.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.data.document.dto.PositionLeaveDto;
import com.data.document.model.PositionLeave;
import com.data.document.service.PositionLeaveService;
import com.data.document.service.PositionService;

@RestController
@RequestMapping("/positionLeave")
public class PositionLeaveController {

	@Autowired
	PositionLeaveService plService;
	
	@Autowired
	PositionService pstService;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping
	public ResponseEntity<HashMap<String, Object>> getData() {
		HashMap<String, Object> data = new HashMap<>();
		try {
			List<PositionLeave> list = plService.getDataAll();
			data.put("data", list);
			data.put("status", HttpStatus.OK);
		} catch (Exception e) {
			data.put("data", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<HashMap<String, Object>> insertData(@RequestBody PositionLeaveDto dto) {
		HashMap<String, Object> data = new HashMap<>();
		PositionLeave pl = modelMapper.map(dto, PositionLeave.class);
		Date now = new Date();
		try {
			pl.setPosition(pstService.getDataById(dto.getPosition()));
			pl.setCreatedBy("Sistem");
			pl.setUpdatedBy("Sistem");
			pl.setCreatedDate(now);
			pl.setUpdatedDate(now);
			plService.inputData(pl);
			
			data.put("data", pl);
			data.put("status", HttpStatus.OK);
		} catch (Exception e) {
			data.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<HashMap<String, Object>> updateData(@RequestBody PositionLeaveDto pl) {
		HashMap<String, Object> data = new HashMap<>();
		Date now = new Date();
		try {
			PositionLeave plExist = plService.getDataById(pl.getIdPositionLeave());
			plExist.setPosition(pl.getPosition() != null ? pstService.getDataById(pl.getPosition()) : plExist.getPosition());
			plExist.setRationOfLeave(pl.getRationOfLeave() != null ? pl.getRationOfLeave() : plExist.getRationOfLeave());
			plExist.setUpdatedBy("Sistem");
			plExist.setUpdatedDate(now);
			
			plService.inputData(plExist);
			data.put("data", plExist);
			data.put("status", HttpStatus.OK);
		} catch (Exception e) {
			data.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@DeleteMapping("/{idPstLeave}")
	public ResponseEntity<HashMap<String, Object>> deleteData(@PathVariable(value="idPstLeave") Long idPstLeave) {
		HashMap<String, Object> data = new HashMap<>();
		try {
			PositionLeave pl = plService.getDataById(idPstLeave);
			plService.deleteData(pl);
			
			data.put("data", pl);
			data.put("status", HttpStatus.OK);
		} catch (Exception e) {
			data.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
}
