package com.data.document.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.data.document.dto.RequestLeaveDto;
import com.data.document.model.BucketApproval;
import com.data.document.model.PositionLeave;
import com.data.document.model.User;
import com.data.document.model.UserLeaveRequest;
import com.data.document.repository.BucketApprovalRepository;
import com.data.document.repository.PositionLeaveRepository;
import com.data.document.repository.UserLeaveRequestRepository;
import com.data.document.repository.UserRepository;
import com.data.document.service.UserLeaveRequestService;
import com.data.document.service.UserService;
import com.data.document.util.ConvertDate;

@RestController
@RequestMapping("/requestLeave")
public class UserLeaveRequestController {
	
	@Autowired
	UserLeaveRequestRepository ulrRepo;
	
	@Autowired
	BucketApprovalRepository baRepo;
	
	@Autowired
	UserService userService;
	
	@Autowired
	PositionLeaveRepository plRepo;
	
	@Autowired
	UserLeaveRequestService ulrService;
	
	@Autowired
	UserRepository userRepo;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping("/remainingDay/{nik}")
	public ResponseEntity<List<Object>> getDataUser(@PathVariable("nik") BigDecimal nik) {
		HashMap<String, Object> data = new HashMap<>();
		List<Object> arr = new ArrayList<>();
		Optional<User> user = userRepo.findByNik(nik);
		
		if (user.isPresent()) {
			List<UserLeaveRequest> ulRequest = ulrRepo.findDataByUserId(user.get().getIdUser());
			if (ulRequest.size() > 0) {
				data.put("remainingDayOff", ulRequest.get(0).getRemainingDaysOff() + " hari/tahun");
			}else {
				PositionLeave pl = plRepo.findDataByPosition(user.get().getPosition().getIdPosition());
				data.put("remainingDayOff", pl.getRationOfLeave() + " hari/tahun");
			}
			data.put("user", user);
		}
		data.put("status", HttpStatus.OK);
		arr.add(data);
		
		return new ResponseEntity<>(arr, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<HashMap<String, Object>> insertData(@RequestBody RequestLeaveDto dto) throws ParseException {
		HashMap<String, Object> data = new HashMap<>();
		UserLeaveRequest ulRequest = modelMapper.map(dto, UserLeaveRequest.class);
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date expFrom = sdf.parse(ConvertDate.convertDate(dto.getLeaveDateFrom().toString()));
		Date expTo = sdf.parse(ConvertDate.convertDate(dto.getLeaveDateTo().toString()));
		Date expNow = sdf.parse(ConvertDate.convertDate(now.toString()));
		Integer day = 0;
		Integer remain = 0;
		Integer remain2 = 0;
		try {
			Date dateFrom = sdf.parse(ConvertDate.convertDate(dto.getLeaveDateFrom().toString()));
			Date dateTo = sdf.parse(ConvertDate.convertDate(dto.getLeaveDateTo().toString()));
			long diffInMillies = Math.abs(dateTo.getTime() - dateFrom.getTime());
		    long totalLeave = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		    BucketApproval ba = baRepo.findDataExist(dto.getUserId());
		    if (ba != null) {
				data.put("Status", HttpStatus.OK);
				data.put("message", "Anda sudah mengajukan permohonan cuti.");
			}else {
				
				if (expFrom.compareTo(expNow) < 0 || expTo.compareTo(expNow) < 0) {
			    	data.put("Status", HttpStatus.OK);
					data.put("message", "Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti anda.");
				}else if (dateFrom.compareTo(dateTo) > 0) {
			    	data.put("Status", HttpStatus.OK);
					data.put("message", "Tanggal yang Anda ajukan tidak valid.");
			    }else {
			    	
			    	List<UserLeaveRequest> idUser = ulrRepo.findDataByUserId(dto.getUserId());
			    	if (idUser.size() > 0) {
						day = (int) (idUser.get(0).getRemainingDaysOff() - totalLeave);
						remain = idUser.get(0).getRemainingDaysOff();
					}else {
						User user = userService.getDataById(dto.getUserId());
						PositionLeave pl = plRepo.findDataByPosition(user.getPosition().getIdPosition());
						day = (int) (pl.getRationOfLeave() - totalLeave);
						remain = pl.getRationOfLeave();
						remain2 = pl.getRationOfLeave();
					}
				    
					if (remain == 0) {
						data.put("Status", HttpStatus.OK);
						data.put("message", "Mohon maaf, jatah cuti Anda telah habis.");
					
					}else if (day < 0) {
						data.put("Status", HttpStatus.OK);
						data.put("message", "Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari "
								+ "tanggal " + ConvertDate.convertDate(dateFrom.toString()) + " sampai " + ConvertDate.convertDate(dateTo.toString()) + " (" + totalLeave + " hari). Jatah cuti Anda yang tersisa adalah " + (idUser != null ? remain : remain2) + " hari.");
					
					}else {
						ulRequest.setStatus("waiting");
						ulRequest.setDateOfFiling(now);
						ulRequest.setRemainingDaysOff(day);
						ulRequest.setUser(userService.getDataById(dto.getUserId()));
						ulRequest.setCreatedBy("System");
						ulRequest.setUpdatedBy("System");
						ulRequest.setCreatedDate(now);
						ulRequest.setUpdatedDate(now);
						ulrRepo.save(ulRequest);
						
						BucketApproval bucketApp = new BucketApproval();
						bucketApp.setStatus("waiting");
						bucketApp.setDateOfFiling(now);
						bucketApp.setUser(userService.getDataById(dto.getUserId()));
						bucketApp.setCreatedBy("System");
						bucketApp.setCreatedDate(now);
						bucketApp.setUpdatedBy("System");
						bucketApp.setUpdatedDate(now);
						baRepo.save(bucketApp);
						
						data.put("Status", HttpStatus.OK);
						data.put("message", "Permohonan Anda sedang diproses.");
					}
			    }
			}
		
		} catch (Exception e) {
			data.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@GetMapping("/listRequestLeave/{userId}/{totalDataPerPage}/{choosenPage}")
	public ResponseEntity<HashMap<String, Object>> getAllReviews(@PathVariable("userId") Long userId, @PathVariable("totalDataPerPage") int totalDataPerPage, @PathVariable("choosenPage") int choosenPage) throws IOException {
		List<Object> jsonObjectList = new ArrayList<>();
		HashMap<String, Object> jsonResponse = new HashMap<>();
		try {
			Page<UserLeaveRequest> listDocPage = ulrService.getListDocPage(userId, choosenPage, totalDataPerPage);
			List<UserLeaveRequest> listDoc = ulrService.getListDoc(userId);
			List<BucketApproval> listBucket = baRepo.findDataBucketAll(userId);
			for (UserLeaveRequest dc : listDocPage) {
				HashMap<String, Object> data = new HashMap<>();
				data.put("userId", dc.getUser().getIdUser());
				data.put("leaveDateFrom", dc.getLeaveDateFrom());
				data.put("leaveDateTo", dc.getLeaveDateTo());
				data.put("description", dc.getDescription());
				data.put("status", dc.getStatus());
				for (BucketApproval bpl : listBucket) {
					if (dc.getUpdatedDate().toString().trim().equals(bpl.getUpdatedDate().toString().trim())) {
						data.put("resolvedBy", bpl.getResolvedBy());
						data.put("resolvedDate", bpl.getResolvedDate());
						data.put("resolvedReason", bpl.getResolverReason());
						data.put("idBucket", bpl.getIdBucket());
					}
				}
				jsonObjectList.add(data);
			}
			jsonResponse.put("items", jsonObjectList);
			jsonResponse.put("totalItems", listDoc.size());
			jsonResponse.put("status", HttpStatus.OK);
		} catch (Exception e) {
			jsonResponse.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			jsonResponse.put("message", e.getMessage());
		}
		return new ResponseEntity<>(jsonResponse, HttpStatus.OK);
	}
	
}
