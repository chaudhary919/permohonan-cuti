package com.data.document.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.data.document.model.Position;
import com.data.document.model.PositionLeave;
import com.data.document.model.User;
import com.data.document.repository.PositionLeaveRepository;
import com.data.document.repository.UserRepository;
import com.data.document.service.PositionService;

@RestController
@RequestMapping("/position")
public class PositionController {

	@Autowired
	PositionService pstService;
	
	@Autowired
	UserRepository usrRepo;
	
	@Autowired
	PositionLeaveRepository plRepo;
	
	@GetMapping
	public ResponseEntity<HashMap<String, Object>> getData() {
		HashMap<String, Object> data = new HashMap<>();
		try {
			List<Position> list = pstService.getDataAll();
			data.put("data", list);
			data.put("status", HttpStatus.OK);
		} catch (Exception e) {
			data.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<HashMap<String, Object>> insertData(@RequestBody Position pst) {
		HashMap<String, Object> data = new HashMap<>();
		Date now = new Date();
		try {
			pst.setCreatedBy("Sistem");
			pst.setUpdatedBy("Sistem");
			pst.setCreatedDate(now);
			pst.setUpdatedDate(now);
			pstService.inputData(pst);
			data.put("data", pst);
			data.put("status", HttpStatus.OK);
		} catch (Exception e) {
			data.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<HashMap<String, Object>> updateData(@RequestBody Position pst) {
		HashMap<String, Object> data = new HashMap<>();
		Date now = new Date();
		try {
			Position pstExist = pstService.getDataById(pst.getIdPosition());
			pstExist.setPost(pst.getPost() != null ? pst.getPost() : pstExist.getPost());
			pstExist.setUpdatedBy("Sistem");
			pstExist.setUpdatedDate(now);
			
			pstService.inputData(pstExist);
			data.put("data", pstExist);
			data.put("status", HttpStatus.OK);
		} catch (Exception e) {
			data.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@DeleteMapping("/{idPosition}")
	public ResponseEntity<HashMap<String, Object>> deleteData(@PathVariable(value="idPosition") Long idPosition) {
		HashMap<String, Object> data = new HashMap<>();
		try {
			Position pst = pstService.getDataById(idPosition);
			List<User> user = usrRepo.findUserByPosition(idPosition);
			for (User user2 : user) {
				user2.setPosition(null);
				usrRepo.save(user2);
			}
			
			PositionLeave pl = plRepo.findDataByPosition(idPosition);
			pl.setPosition(null);
			plRepo.save(pl);
			
			pstService.deleteData(pst);
			data.put("data", pst);
			data.put("status", HttpStatus.OK);
		} catch (Exception e) {
			data.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
}
