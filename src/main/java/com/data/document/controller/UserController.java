package com.data.document.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.data.document.dto.UserDto;
import com.data.document.model.User;
import com.data.document.repository.UserRepository;
import com.data.document.service.PositionService;
import com.data.document.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService usrService;
	
	@Autowired
	UserRepository usrRepo;
	
	@Autowired
	PositionService pstSerice;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping
	public ResponseEntity<HashMap<String, Object>> getData() {
		HashMap<String, Object> data = new HashMap<>();
		try {
			List<User> list = usrService.getDataAll();
			data.put("data", list);
			data.put("status", HttpStatus.OK);
		} catch (Exception e) {
			data.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<HashMap<String, Object>> insertData(@RequestBody UserDto dto) {
		HashMap<String, Object> data = new HashMap<>();
		User user = modelMapper.map(dto, User.class);
		Date now = new Date();
		try {
			Optional<User> userExist = usrRepo.findByNik(dto.getNik());
			if (!userExist.isPresent()) {
				user.setCreatedBy("System");
				user.setUpdatedBy("System");
				user.setCreatedDate(now);
				user.setUpdatedDate(now);
				user.setPosition(pstSerice.getDataById(dto.getPosition()));
				usrService.inputData(user);
				data.put("data", user);
				data.put("status", HttpStatus.OK);
			}else {
				data.put("message", "User Already Exist");
				data.put("status", HttpStatus.OK);
			}
			
		} catch (Exception e) {
			data.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<HashMap<String, Object>> updateData(@RequestBody UserDto user) {
		HashMap<String, Object> data = new HashMap<>();
		Date now = new Date();
		try {
			User usrExist = usrService.getDataById(user.getIdUser());
			usrExist.setNik(user.getNik() != null ? user.getNik() : usrExist.getNik());
			usrExist.setUserName(user.getUserName() != null ? user.getUserName() : usrExist.getUserName());
			usrExist.setAge(user.getAge() != null ? user.getAge() : usrExist.getAge());
			usrExist.setGender(user.getGender() != null ? user.getGender() : usrExist.getGender());
			usrExist.setUpdatedBy("Sistem");
			usrExist.setUpdatedDate(now);
			usrExist.setPosition(user.getPosition() != null ? pstSerice.getDataById(user.getPosition()) : usrExist.getPosition());
			
			usrService.inputData(usrExist);
			data.put("data", usrExist);
			data.put("status", HttpStatus.OK);
		} catch (Exception e) {
			data.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
	
	@DeleteMapping("/{idUser}")
	public ResponseEntity<HashMap<String, Object>> deleteData(@PathVariable(value="idUser") Long idUser) {
		HashMap<String, Object> data = new HashMap<>();
		try {
			User usr = usrService.getDataById(idUser);
			usrService.deleteData(usr);
			data.put("data", usr);
			data.put("status", HttpStatus.OK);
		} catch (Exception e) {
			data.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			data.put("message", e.getMessage());
		}
		return new ResponseEntity<>(data, HttpStatus.OK);
	}
}
