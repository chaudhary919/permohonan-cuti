package com.data.document.dto;

public class PositionLeaveDto {

	private Long idPositionLeave;
	private Long position;
	private Integer rationOfLeave;
	
	public Long getIdPositionLeave() {
		return idPositionLeave;
	}
	public void setIdPositionLeave(Long idPositionLeave) {
		this.idPositionLeave = idPositionLeave;
	}
	public Long getPosition() {
		return position;
	}
	public void setPosition(Long position) {
		this.position = position;
	}
	public Integer getRationOfLeave() {
		return rationOfLeave;
	}
	public void setRationOfLeave(Integer rationOfLeave) {
		this.rationOfLeave = rationOfLeave;
	}
	
}
