package com.data.document.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class BucketApprovalDto {

	private Long idBucket;
	private String status;
	private String resolverReason;
	private String resolvedBy;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date resolvedDate;
	
	public Long getIdBucket() {
		return idBucket;
	}
	public void setIdBucket(Long idBucket) {
		this.idBucket = idBucket;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResolverReason() {
		return resolverReason;
	}
	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}
	public String getResolvedBy() {
		return resolvedBy;
	}
	public void setResolvedBy(String resolvedBy) {
		this.resolvedBy = resolvedBy;
	}
	public Date getResolvedDate() {
		return resolvedDate;
	}
	public void setResolvedDate(Date resolvedDate) {
		this.resolvedDate = resolvedDate;
	}
}
