package com.data.document.dto;

import java.math.BigDecimal;

public class UserDto {

	private Long idUser;
	private BigDecimal nik;
	private String userName;
	private Integer age;
	private String gender;
	private Long position;
	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	public BigDecimal getNik() {
		return nik;
	}
	public void setNik(BigDecimal nik) {
		this.nik = nik;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Long getPosition() {
		return position;
	}
	public void setPosition(Long position) {
		this.position = position;
	}
	
}
