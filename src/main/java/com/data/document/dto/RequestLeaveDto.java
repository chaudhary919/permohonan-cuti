package com.data.document.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class RequestLeaveDto {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date leaveDateFrom;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date leaveDateTo;
	private Long userId;
	private String description;
	
	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}
	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}
	public Date getLeaveDateTo() {
		return leaveDateTo;
	}
	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
