package com.data.document.util;

public class ConvertDate {

	public static String convertDate(String myDate) {
		String strMnth    = myDate.substring(4,7);
		String strDay        = myDate.substring(8,10);
		String strYear       = myDate.substring(24,28);
		String strMonth = "";
		if (strMnth.equals("Jan")) {
			strMonth = "01";
		}else if (strMnth.equals("Feb")) {
			strMonth = "02";
		}else if (strMnth.equals("Mar")) {
			strMonth = "03";
		}else if (strMnth.equals("Apr")) {
			strMonth = "04";
		}else if (strMnth.equals("May")) {
			strMonth = "05";
		}else if (strMnth.equals("Jun")) {
			strMonth = "06";
		}else if (strMnth.equals("Jul")) {
			strMonth = "07";
		}else if (strMnth.equals("Aug")) {
			strMonth = "08";
		}else if (strMnth.equals("Sep")) {
			strMonth = "09";
		}else if (strMnth.equals("Oct")) {
			strMonth = "10";
		}else if (strMnth.equals("Nov")) {
			strMonth = "11";
		}else if (strMnth.equals("Des")) {
			strMonth = "12";
		}
		return strYear+"-"+strMonth+"-"+strDay;
	}
}
