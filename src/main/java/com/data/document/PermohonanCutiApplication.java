package com.data.document;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PermohonanCutiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PermohonanCutiApplication.class, args);
	}

}
