package com.data.document.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="bucket_approval")
@JsonIgnoreProperties({"createdBy", "createdDate", "updatedBy", "updatedDate"})
public class BucketApproval implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_bucket", unique=true, nullable=false)
	private Long idBucket;
	
	@Column(name="status")
	private String status;
	
	@Column(name="date_of_filing")
	@Temporal(TemporalType.DATE)
	private Date dateOfFiling;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private User userId;
	
	@Column(name="resolved_by")
	private String resolvedBy;
	
	@Column(name="resolved_date")
	@Temporal(TemporalType.DATE)
	private Date resolvedDate;
	
	@Column(name="resolver_reason")
	private String resolverReason;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="created_date")
	@Temporal (TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name="updated_by")
	private String updatedBy;
	
	@Column(name="updated_date")
	@Temporal (TemporalType.TIMESTAMP)
	private Date updatedDate;

	public Long getIdBucket() {
		return idBucket;
	}

	public void setIdBucket(Long idBucket) {
		this.idBucket = idBucket;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDateOfFiling() {
		return dateOfFiling;
	}

	public void setDateOfFiling(Date dateOfFiling) {
		this.dateOfFiling = dateOfFiling;
	}

	public User getUser() {
		return userId;
	}

	public void setUser(User user) {
		this.userId = user;
	}

	public String getResolvedBy() {
		return resolvedBy;
	}

	public void setResolvedBy(String decisionMaker) {
		this.resolvedBy = decisionMaker;
	}

	public Date getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(Date dateOfDecisionMaker) {
		this.resolvedDate = dateOfDecisionMaker;
	}

	public String getResolverReason() {
		return resolverReason;
	}

	public void setResolverReason(String reasonForDecision) {
		this.resolverReason = reasonForDecision;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
