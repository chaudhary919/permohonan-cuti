package com.data.document.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="position_leave")
@JsonIgnoreProperties({"createdBy", "createdDate", "updatedBy", "updatedDate"})
public class PositionLeave implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_position_leave", unique=true, nullable=false)
	private Long idPositionLeave;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="position_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Position position;
	
	@Column(name="ration_of_leave")
	private Integer rationOfLeave;
	
	@Column(name="created_by")
	private String createdBy;
	
	@Column(name="created_date")
	@Temporal (TemporalType.TIMESTAMP)
	private Date createdDate;
	
	@Column(name="updated_by")
	private String updatedBy;
	
	@Column(name="updated_date")
	@Temporal (TemporalType.TIMESTAMP)
	private Date updatedDate;

	public Long getIdPositionLeave() {
		return idPositionLeave;
	}

	public void setIdPositionLeave(Long idPositionLeave) {
		this.idPositionLeave = idPositionLeave;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Integer getRationOfLeave() {
		return rationOfLeave;
	}

	public void setRationOfLeave(Integer rationOfLeave) {
		this.rationOfLeave = rationOfLeave;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
