INSERT INTO positions(id_position, created_by, created_date, post, updated_by, updated_date)
    VALUES (nextval('hibernate_sequence'), 'System', now(), 'employee', 'System', now()),
    (nextval('hibernate_sequence'), 'System', now(), 'staff', 'System', now()),
    (nextval('hibernate_sequence'), 'System', now(), 'supervisor', 'System', now());

INSERT INTO position_leave(id_position_leave, created_by, created_date, ration_of_leave, 
            updated_by, updated_date, position_id)
    VALUES (nextval('hibernate_sequence'), 'System', now(), 12, 'System', now(), (select id_position from positions where post = 'employee')),
    (nextval('hibernate_sequence'), 'System', now(), 13, 'System', now(), (select id_position from positions where post = 'staff')),
    (nextval('hibernate_sequence'), 'System', now(), 15, 'System', now(), (select id_position from positions where post = 'supervisor'));
