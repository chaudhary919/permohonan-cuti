-- Table: positions
-- DROP TABLE positions;
CREATE TABLE positions
(
  id_position bigint NOT NULL,
  created_by character varying(255),
  created_date timestamp without time zone,
  post character varying(255),
  updated_by character varying(255),
  updated_date timestamp without time zone,
  CONSTRAINT positions_pkey PRIMARY KEY (id_position)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE positions
  OWNER TO postgres;
  

-- Table: users
-- DROP TABLE users;
CREATE TABLE users
(
  id_user bigint NOT NULL,
  age integer,
  created_by character varying(255),
  created_date timestamp without time zone,
  gender character varying(1),
  nik numeric(19,2),
  updated_by character varying(255),
  updated_date timestamp without time zone,
  user_name character varying(255),
  position_id bigint,
  CONSTRAINT users_pkey PRIMARY KEY (id_user),
  CONSTRAINT fk6ph6xiiydudp6umjf2xckbbmi FOREIGN KEY (position_id)
      REFERENCES positions (id_position) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;


-- Table: position_leave
-- DROP TABLE position_leave;
CREATE TABLE position_leave
(
  id_position_leave bigint NOT NULL,
  created_by character varying(255),
  created_date timestamp without time zone,
  ration_of_leave integer,
  updated_by character varying(255),
  updated_date timestamp without time zone,
  position_id bigint,
  CONSTRAINT position_leave_pkey PRIMARY KEY (id_position_leave),
  CONSTRAINT fkc35rodyuvkdbcn0fka5bdq5fi FOREIGN KEY (position_id)
      REFERENCES positions (id_position) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE position_leave
  OWNER TO postgres;


-- Table: user_leave_request
-- DROP TABLE user_leave_request;
CREATE TABLE user_leave_request
(
  id_leave_request bigint NOT NULL,
  created_by character varying(255),
  created_date timestamp without time zone,
  date_of_filing date,
  description character varying(255),
  leave_date_from date,
  leave_date_to date,
  remaining_days_off integer,
  status character varying(255),
  updated_by character varying(255),
  updated_date timestamp without time zone,
  user_id bigint,
  CONSTRAINT user_leave_request_pkey PRIMARY KEY (id_leave_request),
  CONSTRAINT fknmbdahm888t1iirloauc80tyl FOREIGN KEY (user_id)
      REFERENCES users (id_user) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_leave_request
  OWNER TO postgres;


-- Table: bucket_approval
-- DROP TABLE bucket_approval;
CREATE TABLE bucket_approval
(
  id_bucket bigint NOT NULL,
  created_by character varying(255),
  created_date timestamp without time zone,
  date_of_filing date,
  resolved_by character varying(255),
  resolved_date date,
  resolver_reason character varying(255),
  status character varying(255),
  updated_by character varying(255),
  updated_date timestamp without time zone,
  user_id bigint,
  CONSTRAINT bucket_approval_pkey PRIMARY KEY (id_bucket),
  CONSTRAINT fkh256e89fg344sw9ctkq3av1tc FOREIGN KEY (user_id)
      REFERENCES users (id_user) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE bucket_approval
  OWNER TO postgres;





